<?php

namespace Controller\Base;

use Controller\Interfaces\BasePhotoInterface;

/**
 * Class BasePhotoController
 */
class BasePhotoController implements BasePhotoInterface
{
    /**
     * Photos array.
     *
     * @var array
     */
    private $data = [];

    /**
     * @var integer
     */
    private $id;

    /**
     * BasePhotoController constructor.
     *
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->setId($id);
    }

    /**
     * {@inheritdoc}
     */
    public function setId(string $vkId)
    {
        // Strip non numeric characters.
        $id = preg_replace('/\D/', '', $vkId);

        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        return $this->data;
    }
}