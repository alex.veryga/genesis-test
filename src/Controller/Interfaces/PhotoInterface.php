<?php

namespace Controller\Interfaces;

/**
 * Interface PhotoInterface
 */
interface PhotoInterface
{
    /**
     * Returned list of photos according to vk user ID.
     *
     * @return mixed|array
     */
    public function getPhotos(): array;
}