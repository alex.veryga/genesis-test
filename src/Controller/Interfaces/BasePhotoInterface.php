<?php

namespace Controller\Interfaces;

/**
 * Interface BasePhotoInterface
 *
 * @package Controller\PhotoInterface
 */
interface BasePhotoInterface
{
    /**
     * @param string $id
     */
    public function setId(string $id);

    /**
     * @param array $data
     */
    public function setData(array $data);

    /**
     * @return array $data
     */
    public function getData(): array;
}