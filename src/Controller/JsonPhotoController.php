<?php

namespace Controller;

use Controller\Base\BasePhotoController;
use Controller\Interfaces\PhotoInterface;

/**
 * Class JsonPhotoController
 */
class JsonPhotoController extends BasePhotoController implements PhotoInterface
{
    /**
     * Path to json file with photos data.
     */
    const DATA_FILE_PATH = __DIR__ . '/../../public/photos.json';

    /**
     * {@inheritdoc}
     */
    public function getPhotos(): array
    {
        if (!$this->getId()) {
            throw new \InvalidArgumentException('Empty ID parameter');
        }

        $photos = json_decode(file_get_contents(self::DATA_FILE_PATH), true);

        $userPhotos[$this->getId()] = [];
        foreach($photos['response']['items'] as $item) {
            if ($item['owner_id'] == $this->getId()) {
                // Get photo values only.
                $data = array_filter($item, function($key){
                    return preg_match('#photo_#', $key);
                }, ARRAY_FILTER_USE_KEY);

                $userPhotos[$this->getId()] = array_merge($userPhotos[$this->getId()], array_values($data));
            }
        }

        return $userPhotos;
    }
}