<?php

namespace Command;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Traits\ConsoleTrait;
use Controller\JsonPhotoController;

/**
 * Class GetPhotosCommand
 *
 * @package Command
 */
class GetPhotosCommand extends Command
{
    use ConsoleTrait;

    /**
     * Import queue name
     */
    const QUEUE_NAME = 'photo_queue';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('get:photos')
            ->setDescription('Getting VK photos')
            ->addArgument(
                'id', InputArgument::REQUIRED, 'Your VK ID'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getConsole()->section('Import VK photos:');
        $import = new JsonPhotoController($input->getArgument('id'));

        $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();
        $channel->queue_declare(self::QUEUE_NAME, false, false, false, false);

        foreach ($import->getPhotos() as $userId => $photos) {
            $data = [
                'userId' => $import->getId(),
                'photos' => $photos
            ];

            $msg = new AMQPMessage(json_encode($data), ['delivery_mode' => 2]);
            $channel->basic_publish($msg, '', self::QUEUE_NAME);
            $this->getConsole()->block('Added photos to queue for ID: ' . $import->getId());
        }

        $channel->close();
        $connection->close();

        $this->getConsole()->success('Import completed');
    }
}
