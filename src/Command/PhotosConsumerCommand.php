<?php
namespace Command;

use DB\DB;
use DB\Entity\User;
use PhpAmqpLib\Connection\AMQPConnection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Traits\ConsoleTrait;

class PhotosConsumerCommand extends Command
{
    use ConsoleTrait;

    /**
     * Import queue name
     */
    const QUEUE_NAME = 'photo_queue';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('mq:photos')->setDescription('import VK photos');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getConsole()->section('Handling queue:');

        $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');

        $channel = $connection->channel();
        $channel->queue_declare(self::QUEUE_NAME, false, false, false, false);
        $channel->basic_qos(null, 1, null);
        $channel->basic_consume(self::QUEUE_NAME, '', false, false, false, false, $this->getCallback());

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $this->getConsole()->success('Queue empty');

        $channel->close();
        $connection->close();
    }

    /**
     * RabbitMQ worker.
     *
     * @return \Closure
     */
    protected function getCallback()
    {
        return function ($msg) {
            $data = json_decode($msg->body, true);

            $connection = new DB();
            $entityManager = $connection->getEntityManager();
            $entityManager->getRepository(User::class)->addUserPhotos($data);

            $this->getConsole()->block('Photos for vkID: ' . $data['userId'] . ' imported');
        };
    }
}