<?php

namespace Command;

use DB\DB;
use DB\Entity\Photo;
use DB\Entity\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Traits\ConsoleTrait;

/**
 * Class GetReportCommand
 *
 * @package Command
 */
class GetReportCommand extends Command
{
    use ConsoleTrait;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('get:report')
            ->setDescription('Getting user photos report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = new DB();
        $entityManager = $connection->getEntityManager();
        $users = $entityManager->getRepository(User::class)->findAll();

        /** @var User $user */
        foreach ($users as $user) {
            $this->getConsole()->section('Vkontakte user ID: ' . $user->getVkUserId());

            $i = 1;
            /** @var Photo $photo */
            foreach ($user->getPhotos()->toArray() as $photo) {
                $this->getConsole()->writeln($i . '. ' . $photo->getPhoto());

                $i++;
            }
        }
    }
}
