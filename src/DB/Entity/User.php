<?php

namespace DB\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Project user entity.
 *
 * @ORM\Entity(repositoryClass="\DB\Repository\UserRepository")
 * @ORM\Table(name="user",
 *     uniqueConstraints={@UniqueConstraint(name="vk_user_unique",columns={"vk_user_id"})}
 * )
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $vk_user_id;

    /**
     * @var ArrayCollection|Photo[]
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="user")
     */
    private $photos;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->setPhotos(new ArrayCollection());
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return integer
     */
    public function getVkUserId()
    {
        return $this->vk_user_id;
    }

    /**
     * @param integer $vkUserId
     */
    public function setVkUserId($vkUserId)
    {
        $this->vk_user_id = $vkUserId;
    }

    /**
     * @return ArrayCollection|Photo[]
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param ArrayCollection|Photo[] $photos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
    }
}