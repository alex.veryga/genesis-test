<?php

namespace DB\Repository;

use DB\Entity\Photo;
use DB\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * Add user corresponding photos.
     *
     * @param array $data
     */
    public function addUserPhotos(array $data)
    {
        $vkId = $data['userId'];

        // For multiple testing only
        $this->deleteUserDyVkId($vkId);

        // Add user
        $user = new User();
        $user->setVkUserId($vkId);
        $this->getEntityManager()->persist($user);

        foreach ($data['photos'] as $userPhoto) {
            // Add User photo
            $singlePhoto = new Photo();
            $singlePhoto->setUser($user);
            $singlePhoto->setPhoto($userPhoto);

            $this->getEntityManager()->persist($singlePhoto);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * Delete single user by vk ID.
     *
     * @param $vkId
     */
    protected function deleteUserDyVkId($vkId)
    {
        $this->getEntityManager()->createQueryBuilder('u')
            ->where('u.vk_user_id = :vk_user_id')
            ->setParameter('vk_user_id', $vkId)
            ->delete();

        $this->getEntityManager()->flush();
    }
}