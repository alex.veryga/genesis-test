<?php

namespace DB;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

/**
 * Class DB
 */
class DB
{
    /**
     * Create database connection.
     */
    public function getEntityManager()
    {
        $config = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/Entity'], true, null, null, false);

        $connectionParams = [
            'dbname' => 'genesis_test',
            'user' => 'root',
            'password' => 'dfbn78zx',
            'host' => 'localhost',
            'driver' => 'pdo_mysql',
        ];

        return $entityManager = EntityManager::create($connectionParams, $config);
    }
}