<?php

namespace Service;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Service adds functionality for colorized output to terminal.
 */
class Console extends SymfonyStyle
{
    /**
     * @var bool
     */
    private $consoleShowTimestamp = true;

    /**
     * @var bool
     */
    private $consoleShowMemoryUsage = false;

    /**
     * {@inheritdoc}
     */
    public function __construct(InputInterface $input = null, OutputInterface $output = null)
    {
        $input = $input ?: new StringInput('');
        $output = $output ?: new ConsoleOutput();

        parent::__construct($input, $output);
    }

    /**
     * @param boolean $consoleShowTimestamp
     */
    public function setConsoleShowTimestamp($consoleShowTimestamp)
    {
        $this->consoleShowTimestamp = $consoleShowTimestamp;
    }

    /**
     * @param boolean $consoleShowMemoryUsage
     */
    public function setConsoleShowMemoryUsage($consoleShowMemoryUsage)
    {
        $this->consoleShowMemoryUsage = $consoleShowMemoryUsage;
    }

    /**
     * Pretty stats output.
     *
     * @param array $stats
     */
    public function stats(array $stats)
    {
        $statsArray = [];
        foreach ($stats as $statName => $stat) {
            // Concatenation is used here to avoid PhpStorm's HTML tag name error.
            $statsArray[] = ['<' . "fg=green;options=bold>\xE2\x9C\x94</>", $statName, $stat];
        }
        $this->table([], $statsArray);
    }

    /**
     * {@inheritdoc}
     */
    public function createProgressBar($max = 0)
    {
        $progressBar = parent::createProgressBar($max);

        $progressBar->setEmptyBarCharacter('░░');
        $progressBar->setProgressCharacter('');
        $progressBar->setBarCharacter('▓▓');

        return $progressBar;
    }

    /**
     * @param \Exception $e
     */
    public function exception(\Exception $e)
    {
        $message = sprintf(
            '%s: %s (uncaught exception) at %s line %s',
            get_class($e),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine()
        );

        $this->newLine(2);
        $this->error($message);
    }

    /**
     * {@inheritdoc}
     */
    public function writeln($messages, $type = self::OUTPUT_NORMAL)
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }

        if ($this->consoleShowTimestamp) {
            $messages[0] .= ' [' . date('Y-m-d H:i:s') . ']';
        }

        if ($this->consoleShowMemoryUsage) {
            $messages[0] .= ' (' . memory_get_usage(true) / 1024 / 1024 . 'MB)';
        }

        parent::writeln($messages, $type);
    }
}
