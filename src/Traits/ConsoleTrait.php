<?php

namespace Traits;

use Service\Console;

/**
 * Trait for services that need console output.
 *
 * Class ConsoleTrait
 *
 * @package Traits
 */
trait ConsoleTrait
{
    /**
     * @var Console
     */
    private $console;

    /**
     * @return Console
     */
    protected function getConsole()
    {
        if (!$this->console) {
            $this->console = new Console();
        }

        return $this->console;
    }

    /**
     * @param Console $console
     */
    public function setConsole(Console $console)
    {
        $this->console = $console;
    }
}
