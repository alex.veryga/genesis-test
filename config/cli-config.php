<?php
use DB\DB;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

include_once __DIR__ . '/../vendor/autoload.php';

$conn = new DB();
$entityManager = $conn->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);
